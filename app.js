const express = require("express");

const app = express();
const port = 3000;

const ProductsRouter = require('./middleware/products')
const OrdersRouter = require('./middleware/orders')

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.listen(port, () => {
    console.log(`Server running on port ${port}!`);
});

app.use('/products', ProductsRouter);
app.use('/orders', OrdersRouter);