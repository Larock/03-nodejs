const express = require('express');
const router = express.Router();

const connection = require('../helpers/connection');
const query = require('../helpers/query');
const dbConfig = require('../dbConfig');

const createOrder = "INSERT INTO orders (name, email, status) VALUES (?, ?, ?)";
const addToJoin = "insert INTO product_order (order_id, product_id) VALUES (?, ?)";

router.post('/order', async(req, res) => {
    const { order } = req.params;
    const conn = await connection(dbConfig).catch(e => {console.log(e)});
    await query(conn, createOrder, order).catch(e => {console.log(e)});
    await query(conn, addToJoin, order/*aanpassen*/).catch(e => {console.log(e)});
})

module.exports = router;