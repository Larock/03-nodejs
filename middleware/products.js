const express = require('express');
const router = express.Router();

const connection = require('../helpers/connection');
const query = require('../helpers/query');
const dbConfig = require('../dbConfig');

const getAllProducts = "SELECT * FROM products";
const getProductById = "SELECT * FROM products WHERE id = ?";
const getProductsByCategory = "SELECT * FROM products WHERE category = ?";

//Get all products from database
router.get('/all', async(req, res) => {
  const conn = await connection(dbConfig).catch(e => {console.log(e)});
  const results = await query(conn, getAllProducts).catch(e => {console.log(e)});
  return res.json({results});
});

//Get products from specific category from database
router.get('/category/:category', async(req, res) => {
  const { category } = req.params;
  const conn = await connection(dbConfig).catch(e => {console.log(e)});
  const results = await query(conn, getProductsByCategory, category).catch(e => {console.log(e)});
  return res.json({results});
})

//Get product by id from database
router.get('/id/:id', async (req, res) => {
  const { id } = req.params;
  const conn = await connection(dbConfig).catch(e => {console.log(e)});
  const result = await query(conn, getProductById, id).catch(e => console.log(e));
  return res.json({result});
});

module.exports = router;